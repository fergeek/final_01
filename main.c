#include <stdio.h>
#include <math.h>
int main(int argc, char const *argv[]) {
  double var;
  printf(" \033[1m\033[7m  CALCULAR VARIABLES TRIGONOMETRICAS  \033[0m\n Ingrese un valor: ");
  scanf("%lf", &var);
  printf("  \033[4mSeno:\033[0m \033[1m%.3lf\033[0m\n", sin(var));
  printf("  \033[4mCoseno:\033[0m \033[1m%.3lf\033[0m\n", cos(var));
  printf("  \033[4mTangente:\033[0m \033[1m%.3lf\033[0m\n", tan(var));
  return 0;
}
